#include "rc.h"

#include <errno.h>

bool forked = false;

typedef struct Pid Pid;

static struct Pid {
	Pid *n;
	pid_t pid;
	int stat;
	bool alive;
	bool waiting;
} *plist = NULL;

pid_t rc_fork(void) {
	Pid *new;
	struct Pid *p, *q;
	pid_t pid = fork();

	switch (pid) {
	case -1:
		uerror("fork");
		rc_error(NULL);
		/* NOTREACHED */
	case 0:
		forked = true;
		sigchk();
		clearflow();
		p = plist; q = 0;
		while (p) {
			if (q) efree(q);
			q = p;
			p = p->n;
		}
		if (q) efree(q);
		plist = 0;
		return 0;
	default:
		new = enew(Pid);
		new->pid = pid;
		new->alive = true;
		new->waiting = false;
		new->n = plist;
		plist = new;
		return pid;
	}
}

static int markwaiting(pid_t pid, bool clear) {
	Pid *p;
	int n = 0;
	for (p = plist; p != NULL; p = p->n)
		if (pid == -1 || p->pid == pid) {
			p->waiting = true;
			n++;
		} else if (clear)
			p->waiting = false;
	return n;
}

static pid_t dowait(int *stat, bool nointr) {
	Pid **p, *q;
	pid_t pid;
	for (;;) {
		for (p = &plist; *p != NULL; p = &(*p)->n) {
			q = *p;
			if (q->waiting && !q->alive) {
				pid = q->pid;
				*stat = q->stat;
				*p = q->n; /* remove element from list */
				efree(q);
				return pid;
			}
		}
		pid = rc_wait(stat);
		if (pid < 0) {
			if (errno == ECHILD)
				panic("lost child");
			if (nointr)
				continue;
			else
				return pid;
		}
		for (q = plist; q != NULL; q = q->n)
			if (q->pid == pid) {
				q->alive = false;
				q->stat = *stat;
				break;
			}
	}
	/* never reached */
	return -1;
}

pid_t rc_wait4(pid_t pid, int *stat, bool nointr) {
	if (markwaiting(pid, true) == 0) {
		/* Uh-oh, not there. */
		errno = ECHILD; /* no children */
		uerror("wait");
		*stat = 0x100; /* exit(1) */
		return -1;
	}
	return dowait(stat, nointr);
}

List *sgetapids(void) {
	List *r;
	Pid *p;
	for (r = NULL, p = plist; p != NULL; p = p->n) {
		List *q;
		if (!p->alive)
			continue;
		q = nnew(List);
		q->w = nprint("%d", p->pid);
		q->m = NULL;
		q->n = r;
		r = q;
	}
	return r;
}

void waitforall(void) {
	int stat;
	markwaiting(-1, true);
	while (plist != NULL) {
		pid_t pid = dowait(&stat, false);
		if (pid > 0)
			setstatus(pid, stat);
		else {
			set(false);
			if (errno == EINTR)
				return;
		}
		sigchk();
	}
}

void waitfor(char **av) {
	int alive, count, i, stat;
	pid_t pid;
	for (i = 0; av[i] != NULL; i++)
		if ((pid = a2u(av[i])) < 0) {
			efprint("`%s' is a bad number\n", av[i]);
			set(false);
			return;
		} else if (markwaiting(pid, i == 0) == 0) {
			efprint("`%s' is not a child\n", av[i]);
			set(false);
			return;
		}
	alive = count = i;
	if (count >= MAX_PIPELINE) {
		efprint("too many arguments to wait\n");
		set(false);
		return;
	}
	setpipestatuslength(count);
	while (alive > 0) {
		pid = dowait(&stat, false);
		if (pid > 0) {
			alive--;
			for (i = 0; av[i] != NULL; i++)
				if (a2u(av[i]) == pid) {
					setpipestatus(count - i - 1, pid, stat);
					break;
				}
		} else if (errno == EINTR) {
			set(false);
			return;
		}
		sigchk();
	}
}

