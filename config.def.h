/* Define if you want rc23 to hand off exec errors to (e.g.) /bin/sh. */
#define DEFAULTINTERP "/bin/sh"

/* Define to the default path used if $PATH is empty when rc23 starts. */
#define DEFAULTPATH "/usr/local/bin","/usr/bin","/bin"

/* Define if your kernel supports `#!' magic numbers */
#define HASH_BANG 1

/* Define if you want rc23 to encode strange characters in the environment. */
#define PROTECT_ENV 1

/* Define if you want echo as a builtin. */
#define RC_ECHO 1

/* Define if you want read as a builtin */
#define RC_READ 1

/* Define if you want rc23 to support broken apps, like a job control shell. */
#define RC_JOB 1
