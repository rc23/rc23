#!/usr/bin/env sh
# Detect system properties.
# Properties can be overridden with CFLAGS.

def() {
	echo "
/* $2 */
#ifndef $1
#define $1 1
#endif"
}

test -d /dev/fd && def HAVE_DEV_FD 'System supports /dev/fd'
test -d /proc/self/fd && def HAVE_PROC_SELF_FD 'System supports /proc/self/fd'
