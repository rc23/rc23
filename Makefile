.POSIX: # 2024

PREFIX ?=	/usr/local
MANPREFIX ?=	$(PREFIX)/share/man

CC ?=		cc
CFLAGS ?=	-O -Wall -g
CPPFLAGS ?=	-I$(PREFIX)/include
LDFLAGS ?=	-L$(PREFIX)/lib
YACC ?=		yacc

# line editing library: null/edit/editline/readline
EDIT =		readline

# include extra builtins in addon.c
RC_ADDON = 	0

-include local.mk

CPPFLAGS +=	-DRC_ADDON=$(RC_ADDON)

LIB_EDIT_null =
LIB_EDIT_edit = -ledit
LIB_EDIT_editline = -leditline
LIB_EDIT_readline = -lreadline
LDLIBS += $(LIB_EDIT_$(EDIT))

OBJ_ADDON_0 =
OBJ_ADDON_1 = addon.o
OBJS = $(OBJ_ADDON_$(RC_ADDON)) builtins.o develop.o \
  edit-$(EDIT).o except.o exec.o fn.o footobar.o getopt.o glob.o glom.o \
  hash.o heredoc.o input.o lex.o list.o main.o match.o nalloc.o open.o \
  parse.o print.o redir.o sigmsgs.o signal.o status.o system.o tree.o \
  utils.o var.o wait.o walk.o which.o
HDRS = addon.h edit.h input.h proto.h rc.h rlimit.h version.h
BINS = history mksignal mkstatval tripping

all: rc23

.PHONY: all analyze check clean distclean install sanitize trip
.SUFFIXES:
.SUFFIXES: .c .o
$(V).SILENT:

.c.o:
	@echo "CC $@"
	$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<

.c:
	@echo "LINK $@"
	$(CC) $(LDFLAGS) $(CFLAGS) $(CPPFLAGS) -o $@ $<

rc23: $(OBJS)
	@echo "LINK $@"
	$(CC) $(LDFLAGS) $(CFLAGS) -o $@ $(OBJS) $(LDLIBS)

analyze:
	$(MAKE) CFLAGS='-std=c99 -O2 -fanalyzer -Wall -Wextra -pedantic -Wformat=2 -Wtrampolines' all

# https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html
sanitize:
	$(MAKE) CFLAGS='-std=c99 -O2 -Wall -fno-omit-frame-pointer -fno-common -fsanitize=address,pointer-compare,pointer-subtract,undefined,bounds-strict,leak,signed-integer-overflow' all
	@echo ASAN_OPTIONS=strict_string_checks=1:detect_stack_use_after_return=1:check_initialization_order=1:strict_init_order=1:detect_invalid_pointer_pairs=2

$(OBJS): Makefile $(HDRS) config.h

config.h: config.def.h detect.sh
	@echo "GEN $@"
	cp config.def.h $@
	sh detect.sh >> $@

version.h: VERSION .git/index
	@echo "GEN $@"
	printf '\043define VERSION "%s"\n' "`cat VERSION`" >$@
	printf '\043define DESCRIPTION "%s"\n' "$$(git describe --abbrev=10 --long --dirty --always || echo `cat VERSION`-release)" >>$@

.git/index:

lex.o parse.o: parse.c

# Only rebuild the parser on demand to avoid mtime issues.
parse.c:
	@echo "YACC $@"
	@if $(YACC) -v 2>&1 | grep -q bison; then \
	  $(YACC) $(YFLAGS) -o $@ -d $*.y ;\
	else                   \
	  $(YACC) $(YFLAGS) -b $* -d $*.y ;\
	  mv $*.tab.c $*.c    ;\
	  mv $*.tab.h $*.h    ;\
	fi

builtins.o fn.o hash.o sigmsgs.o signal.o status.o: sigmsgs.c

sigmsgs.c: mksignal
	@echo "GEN $@"
	./mksignal

status.o builtins.o: statval.h

statval.h: mkstatval
	@echo "GEN $@"
	./mkstatval >$@

$(BINS): Makefile proto.h

check: trip
	./rc23 -n p9p-test.rc

trip: rc23 tripping
	./rc23 -p < trip.rc

testhist: history rc23
	cd test-history && ../rc23 -p run.rc

clean:
	rm -f *.o $(BINS) rc23

distclean: clean
	rm -f config.h sigmsgs.[ch] statval.h version.h

install_history: history
	@echo "INSTALL history commands"
	for name in - -p -- --p; do \
	  cp -f history $(DESTDIR)$(PREFIX)/bin/$$name && \
	  chmod 755 $(DESTDIR)$(PREFIX)/bin/$$name; \
	done
	@echo "INSTALL history.1"
	cp history.1 $(DESTDIR)$(MANPREFIX)/man1/
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/history.1

install: all
	@echo "INSTALL bin/rc23"
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f rc23 $(DESTDIR)$(PREFIX)/bin/
	chmod 755 $(DESTDIR)$(PREFIX)/bin/rc23
	@echo "INSTALL rc23.1"
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp rc23.1 $(DESTDIR)$(MANPREFIX)/man1/
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/rc23.1
	@if test -x history; then $(MAKE) install_history; fi

uninstall:
	@echo "RM rc23"
	rm -f $(DESTDIR)$(PREFIX)/bin/rc23
	rm -f $(DESTDIR)$(MANPREFIX)/man1/rc23.1
	@echo "RM history"
	for name in - -p -- --p; do \
	  rm -f $(DESTDIR)$(PREFIX)/bin/$$name; \
	done
	rm -f $(DESTDIR)$(MANPREFIX)/man1/history.1
