/* initialize the input stack */
void initinput(void);

/* push an input onto the stack */
void pushfd(int);
/* the Boolean argument affects line number reporting */
void pushstring(char **, bool);

/* pop the stack */
void popinput(void);

/* get / unget the next character */
int gchar(void);
void ugchar(int);

/* $TERM or $TERMCAP has changed */
void termchange(void);

/* parse a function from the environment */
Node *parseline(char *);

/* main parsing loop; Boolean says whether to exec also */
Node *doit(bool);

/* error recovery: skip to the next newline */
void skiptonl(void);

/* prepare for next line of input */
void nextline(void);

/* close all file descriptors on the stack */
void closefds(void);

/* the last character read */
extern int lastchar;
