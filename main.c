/* main.c: handles initialization of rc and command line options */

#include "rc.h"

#include <errno.h>
#include <locale.h>

#include "input.h"

extern char **environ;

bool dashdee, dashee, dasheye, dashell, dashen, dashoh;
bool dashpee, dashess, dashTEE, dashyew, dashvee, dashex;
bool interactive;
static bool dashEYE;
char *dashsee[2];
pid_t rc_pid;
pid_t rc_ppid;


static void assigndefault(char *,...);
static void checkfd(int, enum redirtype);

int main(int argc, char *argv[], char *envp[]) {
	char *dollarzero, *null[1];
	int c;
	initprint();
	dashsee[0] = dashsee[1] = NULL;
	dollarzero = argv[0];
	rc_pid = getpid();
	rc_ppid = getppid();
	dashell = (*argv[0] == '-'); /* Unix tradition */
	while ((c = rc_getopt(argc, argv, "c:deiIlnopsuvx")) != -1)
		switch (c) {
		case 'c':
			dashsee[0] = rc_optarg;
			goto quitopts;
		case 'd':
			dashdee = true;
			break;
		case 'e':
			dashee = true;
			break;
		case 'I':
			dashEYE = true;
			interactive = false;
			break;
		case 'i':
			dasheye = interactive = true;
			break;
		case 'l':
			dashell = true;
			break;
		case 'n':
			dashen = true;
			break;
		case 'o':
			dashoh = true;
			break;
		case 'p':
			dashpee = true;
			break;
		case 'u':
			dashyew = true;
			break;
		case 's':
			dashess = true;
			break;
		case 'T':
			dashTEE = true;
			break;
		case 'v':
			dashvee = true;
			break;
		case 'x':
			dashex = true;
			break;
		case '?':
			exit(1);
		}
quitopts:
	argv += rc_optind;
	/* use isatty() if neither -i nor -I is set, and if the input is not
	 * from a script or -c flags */
	if (!dasheye && !dashEYE && dashsee[0] == NULL &&
			(dashess || *argv == NULL))
		interactive = isatty(0);
	if (!dashoh) {
		checkfd(0, rFrom);
		checkfd(1, rCreate);
		checkfd(2, rCreate);
	}
	initsignal();
	inithash();
	initparse();
	assigndefault("ifs", " ", "\t", "\n", NULL);
	assigndefault("ofs", " ", NULL);
	assigndefault("nl", "\n", NULL);
#ifdef DEFAULTPATH
	assigndefault("path", DEFAULTPATH, NULL);
#endif
	assigndefault("pid", nprint("%d", rc_pid), NULL);
	assigndefault("ppid", nprint("%d", rc_ppid), NULL);
	assigndefault("prompt", "; ", "", NULL);
	assigndefault("tab", "\t", NULL);
	assigndefault("version", VERSION, "rc23 " DESCRIPTION, NULL);
	assigndefault("noexport",
		"noexport", "apid", "apids", "bqstatus", "cdpath", "home",
		"ifs", "ofs", "path", "pid", "ppid", "status", "*", NULL);
	initenv(envp);
	initinput();
	null[0] = NULL;
	starassign(dollarzero, null, false); /* assign $0 to $* */
	inithandler();

	if (dashell) {
		char *rcrc, *config_dir, *xdgrc;
		int fd;

		xdgrc = (config_dir = getenv("XDG_CONFIG_HOME"))
			? concat(word(config_dir, NULL), word("/rc23/rcrc", NULL) )->w
			: concat(varlookup("home"), word("/.config/rc23/rcrc", NULL))->w;

		rcrc = (access(xdgrc, R_OK) == 0)
			? xdgrc
			: concat(varlookup("home"), word("/.rcrc", NULL))->w;
		fd = rc_open(rcrc, rFrom);
		if (fd == -1) {
			if (errno != ENOENT)
				uerror(rcrc);
		} else {
			bool push_interactive = interactive;
			char *old = currfile(rcrc);
			pushfd(fd);
			interactive = false;
			doit(true);
			interactive = push_interactive;
			currfile(old);
			close(fd);
		}
	}
	environ = makeenv();
	setlocale(LC_CTYPE, "");

	if (dashsee[0] != NULL || dashess) {	/* input from  -c or -s? */
		if (*argv != NULL)
			starassign(dollarzero, argv, false);
		if (dashess) {
			currfile("stdin");
			pushfd(0);
		} else {
			currfile("inline");
			pushstring(dashsee, true);
		}
	} else if (*argv != NULL) {	/* else from a file? */
		b_dot(--argv);
		rc_exit(getstatus());
	} else {			/* else stdin */
		currfile("stdin");
		pushfd(0);
	}
	dasheye = false;
	doit(true);
	rc_exit(getstatus());
	return 0; /* Never really reached. */
}

static void assigndefault(char *name,...) {
	va_list ap;
	List *l;
	char *v;
	va_start(ap, name);
	for (l = NULL; (v = va_arg(ap, char *)) != NULL;)
		l = append(l, word(v, NULL));
	varassign(name, l, false);
	set_exportable(name, false);
	if (streq(name, "path"))
		alias(name, l, false);
	va_end(ap);
}

/* open an fd on /dev/null if it is inherited closed */

static void checkfd(int fd, enum redirtype r) {
	int new = rc_open("/dev/null", r);
	if (new != fd && new != -1)
		close(new);
}
