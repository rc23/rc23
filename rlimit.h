/* What a mess.  This file attempts to straighten everything out. */

#if HAVE_SETRLIMIT

#include <sys/time.h>
#ifdef __hpux
#define _KERNEL
#endif
#include <sys/resource.h>
#ifdef __hpux
#undef _KERNEL
#endif

/* Old BSD syntax. */
#if defined(RLIMIT_OFILE) && !defined(RLIMIT_NOFILE)
#define RLIMIT_NOFILE RLIMIT_OFILE
#endif

/* AIX uses RLIMIT_THREADS, while IRIX uses RLIMIT_PTHREAD */
#if defined(RLIMIT_PTHREAD) && !defined(RLIMIT_THREADS)
#define RLIMIT_THREADS RLIMIT_PTHREAD
#endif
/* and NetBSD uses RLIMIT_NTHR since v7.0. */
#if defined(RLIMIT_NTHR) && !defined(RLIMIT_THREADS)
#define RLIMIT_THREADS RLIMIT_NTHR
#endif

typedef struct Suffix {
	struct Suffix *next;
	long amount;
	char *name;
} Suffix;

typedef struct Limit {
	char *name;
	int flag;
	Suffix *suffix;
	char *desc;
} Limit;

static Suffix
	kbsuf = { NULL, 1024, "k" },
	mbsuf = { &kbsuf, 1024*1024, "m" },
	gbsuf = { &mbsuf, 1024*1024*1024, "g" },
	ussuf = { NULL, 1, "us" },
	stsuf = { NULL, 1, "s" },
	mtsuf = { &stsuf, 60, "m" },
	htsuf = { &mtsuf, 60*60, "h" };
#define	SIZESUF &gbsuf
#define	TIMESUF &htsuf
#define USECSUF &ussuf /* microseconds only */
#define	NOSUF   NULL

static Limit limits[] = {
#ifdef RLIMIT_AIO_MEM /* hpux 11 */
	{ "aiomem",        RLIMIT_AIO_MEM, SIZESUF, "lockable memory by AIO requests" },
#endif
#ifdef RLIMIT_AIO_OPS /* hpux 11 */
	{ "aioops",        RLIMIT_AIO_OPS, NOSUF, "number of AIO ops per process" },
#endif
#ifdef RLIMIT_TCACHE /* hpux 10 only */
	{ "cachedthreads", RLIMIT_TCACHE, NOSUF, "number of cached threads" },
#endif
	{ "coredumpsize",  RLIMIT_CORE, SIZESUF, "core file max size" },
	{ "cputime",       RLIMIT_CPU, TIMESUF, "CPU time limit per process" },
	{ "datasize",      RLIMIT_DATA, SIZESUF, "size of the process data segment" },
#ifdef RLIMIT_NOFILE  /* SUSv2, but not universal */
	{ "descriptors",   RLIMIT_NOFILE, NOSUF, "number of file descriptors" },
#endif
#if defined(__linux) && defined(RLIMIT_LOCKS)
	{ "filelocks",     RLIMIT_LOCKS, NOSUF, "unused since Linux 2.4.25" },
#elif defined(RLIMIT_POSIXLOCKS) /* DragonFly BSD */
	{ "filelocks",     RLIMIT_POSIXLOCKS, NOSUF, "POSIX advisory locks per user" },
#endif
	{ "filesize",      RLIMIT_FSIZE, SIZESUF, "file size a process can create" },
#ifdef RLIMIT_TIME /* MirBSD */
	{ "humantime",     RLIMIT_TIME, TIMESUF, "wallclock time limit per process" },
#endif
#ifdef RLIMIT_KQUEUES /* FreeBSD */
	{ "kqueues",       RLIMIT_KQUEUES, NOSUF, "number of kqueues per user" },
#endif
#ifdef RLIMIT_NPROC
	{ "maxproc",       RLIMIT_NPROC, NOSUF, "number of processes per user" },
#endif
#ifdef RLIMIT_MEMLOCK
	{ "memorylocked",  RLIMIT_MEMLOCK, SIZESUF, "quantity of lockable memory per process" },
#endif
#ifdef RLIMIT_VMEM /* Old Solaris makes a distinction between VMEM and AS */
# if !defined(RLIMIT_AS) || RLIMIT_AS != RLIMIT_VMEM
	{ "memorymap",     RLIMIT_VMEM, SIZESUF, "memory-mapped address size" },
# endif
#endif
#ifdef RLIMIT_RSS /* Usually not enforced, or deprecated */
	{ "memoryrss",     RLIMIT_RSS, SIZESUF, "size of the process resident set" },
#endif
#ifdef RLIMIT_AS /* SUSv2, but not universal */
	{ "memoryuse",     RLIMIT_AS, SIZESUF, "usable virtual memory" },
#endif
#ifdef RLIMIT_MSGQUEUE
	{ "msgqueue",      RLIMIT_MSGQUEUE, SIZESUF, "total size of message queues per user" },
#endif
#ifdef RLIMIT_NICE
	{ "niceness",      RLIMIT_NICE, NOSUF, "max nice value (20 - limit)" },
#endif
#ifdef RLIMIT_NPTS /* FreeBSD */
	{ "nopts",         RLIMIT_NPTS, NOSUF, "number of pseudo-terminals per user" },
#endif
#ifdef RLIMIT_NOVMON /* Haiku  */
	{ "novmon",        RLIMIT_NOVMON, NOSUF, "number of open vnode monitors" },
#endif
#ifdef RLIMIT_RTPRIO
	{ "rtprio",        RLIMIT_RTPRIO, NOSUF, "max real-time priority" },
#endif
#ifdef RLIMIT_RTTIME
	{ "rttime",        RLIMIT_RTTIME, USECSUF, "CPU time in microseconds for RT tasks" },
#endif
#ifdef RLIMIT_SIGPENDING
	{ "sigpending",    RLIMIT_SIGPENDING, NOSUF, "number of queued signals per user" },
#endif
#ifdef RLIMIT_SBSIZE /* FreeBSD, NetBSD */
	{ "socketbufsize", RLIMIT_SBSIZE, SIZESUF, "socket buffer usage per user" },
#endif
	{ "stacksize",     RLIMIT_STACK, SIZESUF, "size of the process stack" },
#ifdef RLIMIT_SWAP /* FreeBSD */
	{ "swapsize",      RLIMIT_SWAP, SIZESUF, "swap size limit per user" },
#endif
#ifdef RLIMIT_THREADS
	{ "threads",       RLIMIT_THREADS, NOSUF, "number of threads per process" },
#endif
	{ NULL, 0, NULL, NULL }
};

#endif /* HAVE_SETRLIMIT */
