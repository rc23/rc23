/* builtins.c: the collection of rc23's builtin commands */

/*
	NOTE: rc23's builtins do not call "rc_error" because they are
	commands, and rc23 errors usually arise from syntax errors.
	Instead, builtins set $status, which may raise an error
	if flag -e is set.
*/

#include "rc.h"

#include <sys/ioctl.h>
#include <errno.h>
#include <limits.h>

#include "addon.h"
#include "input.h"
#include "rlimit.h"
#include "sigmsgs.h"
#include "statval.h"

static void b_break(char **), b_cd(char **), b_continue(char **), b_eval(char **),
  b_false(char **), b_flag(char **), b_exit(char **), b_newpgrp(char **),
  b_return(char **), b_rfork(char **), b_shift(char **), b_true(char **),
  b_umask(char **), b_wait(char **), b_whatis(char **);

#if HAVE_SETRLIMIT
static void b_limit(char **);
#endif

#if RC_ECHO
static void b_echo(char **);
#endif

#if RC_READ
static void b_read(char **);
#endif

static struct {
	builtin_t *p;
	char *name;
} builtins[] = {
	{ b_break,	"break" },
	{ b_builtin,	"builtin" },
	{ b_cd,		"cd" },
	{ b_continue,	"continue" },
#if RC_ECHO
	{ b_echo,	"echo" },
#endif
	{ b_eval,	"eval" },
	{ b_exec,	"exec" },
	{ b_exit,	"exit" },
	{ b_false,	"false" },
	{ b_flag,	"flag" },
#if HAVE_SETRLIMIT
	{ b_limit,	"limit" },
#endif
	{ b_newpgrp,	"newpgrp" },
#if RC_READ
	{ b_read,	"read" },
#endif
	{ b_return,	"return" },
	{ b_rfork,	"rfork" },
	{ b_shift,	"shift" },
	{ b_true,	"true" },
	{ b_umask,	"umask" },
	{ b_wait,	"wait" },
	{ b_whatis,	"whatis" },
	{ b_dot,	"." },
#ifdef ADDONS
	ADDONS
#endif
};

builtin_t *isbuiltin(char *s) {
	for (int i = 0; i < arraysize(builtins); i++)
		if (streq(builtins[i].name, s))
			return builtins[i].p;
	return NULL;
}

/* Inserts `shift n` at the beginning of a function
   to skip the named arguments. */
static void insert_shift(Node *fnargs, char **av, char *fnname) {
	size_t argc = 0, expected = (size_t)fnargs->u[0].i;
	for (; av[argc] != NULL; argc++)
		;

	/* If flag -u is set, raise an error if arguments are missing. */
	if (dashyew && argc < expected)
		rc_error(nprint("function '%s' was called with less "
			"than %ld arguments", fnname, expected));

	/* Avoid shifting more than argc or it will fail. */
	size_t to_shift = MIN(argc, expected);
	if (to_shift == 0) {
		/* No args given, nothing to do. */
		return;
	}
	assert(to_shift < INT_MAX);

	Node *brace = fnargs;
	Node *shift = mk(nWord, "shift", NULL, false);
	Node *narg = mkwordi((int)to_shift);

	/* fnargs has a tree of nPre, followed by nBrace. */
	while(brace->type != nBrace)
		brace = brace->u[1].p;
	
	brace->u[0].p = mk(nBody, mk(nArgs, shift, narg), brace->u[0].p);
}

/* funcall() is the wrapper used to invoke shell functions. pushes $*, and "return" returns here. */

void funcall(char **av) {
	Jbwrap j;
	Estack e1, e2;
	Edata jreturn, star;
	if (sigsetjmp(j.j, 1))
		return;
	Node *fn = treecpy(fnlookup(*av), nalloc);
	if (fn->type == nFnargs)
		insert_shift(fn, av+1, *av);
	starassign(*av, av+1, true);
	jreturn.jb = &j;
	star.name = "*";
	except(eReturn, jreturn, &e1);
	except(eVarstack, star, &e2);
	walk(fn, true);
	varrm("*", true);
	unexcept(eVarstack);
	unexcept(eReturn);
}

static void arg_count(char *name) {
	efprint("too many arguments to %s\n", name);
	set(false);
}

static void badnum(char *num) {
	efprint("`%s' is a bad number\n", num);
	set(false);
}

/* a dummy command. (exec() performs "exec" simply by not forking) */

void b_exec(char **av) {
	USED(av);
}

#if RC_ECHO
/* echo -n omits a newline. echo -- -n echos '-n' */

static void b_echo(char **av) {
	char *format = "%A\n";
	if (*++av != NULL) {
		if (streq(*av, "-n"))
			format = "%A", av++;
		else if (streq(*av, "--"))
			av++;
	}
	fprint(1, format, av);
	set(true);
}
#endif

#if RC_READ

/* Read and print a single line from stdin, including the trailing \n.
   Fails on IO error or if nothing was read.
*/
static void b_read(char **av) {
	if (av[1] != NULL) {
		arg_count("read");
		return;
	}
	
	char *line = NULL, c = 0;
	size_t len = 0, cap = 0;
	ssize_t n;

	while (c != '\n') {
		n = rc_read(0, &c, 1);
		if (n < 0) {
			uerror("read");
			efree(line);
			set(false);
			return;
		}
		if (n == 0 || c == 0)
			break;
		if (cap < len+2)
			line = erealloc(line, cap+=512);
		line[len++] = c;
	}

	if (len > 0) {
		line[len] = 0;
		writeall(1, line, len);
		efree(line);
		set(true);
	} else {
		set(false);
	}
}

#endif

/* cd. traverse $cdpath if the directory given is not an absolute pathname */

static void b_cd(char **av) {
	List *s, nil;
	char *path = NULL;
	size_t t, pathlen = 0;
	if (*++av == NULL) {
		s = varlookup_safe("home");
		*av = (s == NULL) ? "/" : s->w;
	} else if (av[1] != NULL) {
		arg_count("cd");
		return;
	}
	if (isabsolute(*av) || streq(*av, ".") || streq(*av, "..")) { /* absolute pathname? */
		if (chdir(*av) < 0) {
			set(false);
			uerror(*av);
		} else
			set(true);
	} else {
		s = varlookup_safe("cdpath");
		if (s == NULL) {
			s = &nil;
			nil.w = "";
			nil.n = NULL;
		}
	do {
		if (s != &nil && *s->w != '\0') {
			t = strlen(*av) + strlen(s->w) + 2;
			if (t > pathlen || path == NULL)
				path = nalloc(pathlen = t);
			strcpy(path, s->w);
			if (!streq(s->w, "/")) /* "//" is special to POSIX */
				strcat(path, "/");
			strcat(path, *av);
		} else {
			pathlen = 0;
			path = *av;
		}
		if (chdir(path) >= 0) {
			set(true);
			if (interactive && *s->w != '\0' && !streq(s->w, "."))
				fprint(1, "%s\n", path);
			return;
		}
		s = s->n;
	} while (s != NULL);
		efprint("could not cd to `%s'\n", *av);
		set(false);
	}
}

static void b_umask(char **av) {
	mode_t i;
	if (*++av == NULL) {
		set(true);
		i = umask(0);
		umask(i);
		fprint(1, "0%o\n", i);
	} else if (av[1] == NULL) {
		i = (mode_t)o2u(*av);
		if (i > 0777) {
			efprint("bad umask\n");
			set(false);
		} else {
			umask(i);
			set(true);
		}
	} else {
		arg_count("umask");
		return;
	}
}

static void b_exit(char **av) {
	if (*++av != NULL)
		ssetstatus(av);
	rc_exit(getstatus());
}

static void xflag(bool *flag, int mode, int idx, bool readonly) {
	int status = STATUS1;
	if (mode == 2) {
		status = *flag ? STATUS0 : STATUS1;
	} else if (readonly) {
		efprint("flag immutable\n");
	} else {
		*flag = (bool)mode;
		status = STATUS0;
	}
	setpipestatus(idx, -1, status);
}

static void b_flag(char **av) {
	int mode = 3; /* 0 = reset (-), 1 = set (+), 2 = test */
	bool ro = true, rw = false;

	if (*++av == NULL) {
		efprint("not enough arguments to flag\n");
		set(false);
		return;
	}

	char *flags = *av;
	if (flags[0] == '\0') goto flag_usage;
	if (*++av == NULL) {
		mode = 2;
	} else if (av[0][0] == '+' && av[0][1] == '\0') {
		mode = 1;
	} else if (av[0][0] == '-' && av[0][1] == '\0') {
		mode = 0;
	} else {
		goto flag_usage;
	}

	int nflags = (int)strlen(flags);
	if (nflags >= MAX_PIPELINE || nflags < 1)
		goto flag_usage;
	setpipestatuslength(nflags);

	for(int i = 0; i < nflags; i++) {
		int j = nflags - 1 - i;
		switch (flags[i]) {
		case 'c': {
			bool f = dashsee[0] != NULL;
			xflag(&f, mode, i, ro); break;
		}
		case 'd': xflag(&dashdee, mode, j, ro); break;
		case 'e': xflag(&dashee, mode, j, rw); break;
		case 'i': xflag(&interactive, mode, j, rw); break;
		case 'l': xflag(&dashell, mode, j, ro); break;
		case 'n': xflag(&dashen, mode, j, ro); break;
		case 'o': xflag(&dashoh, mode, j, ro); break;
		case 'p': xflag(&dashpee, mode, j, ro); break;
		case 's': xflag(&dashess, mode, j, ro); break;
		case 'T': xflag(&dashTEE, mode, j, rw); break;
		case 'u': xflag(&dashyew, mode, j, rw); break;
		case 'v': xflag(&dashvee, mode, j, rw); break;
		case 'x': xflag(&dashex, mode, j, rw); break;
		default:
			efprint("unknown flag '%c'\n", flags[i]);
			setpipestatus(i, -1, STATUS1);
		}
	}
	return;
flag_usage:
	efprint("flag: invalid arguments\n");
	fprint(2, "usage: flag [cdeilnopsuvx] [+|-]\n");
	set(false);
}

/* raise a "return" exception, i.e., return from a function. if an integer argument is present, set $status to it */

static void b_return(char **av) {
	if (*++av != NULL)
		ssetstatus(av);
	int s = getstatus();
	if (s != 0 && dashee && !cond)
		rc_exit(s);
	rc_raise(eReturn);
}

/* raise a "break" exception for breaking out of for and while loops */

static void b_break(char **av) {
	if (av[1] != NULL) {
		arg_count("break");
		return;
	}
	rc_raise(eBreak);
}

/* raise a "continue" exception to finish early an iteration of 'for' and 'while' loops */

static void b_continue(char **av) {
	if (av[1] != NULL) {
		arg_count("continue");
		return;
	}
	rc_raise(eContinue);
}

/* shift $* n places (default 1) */

static void b_shift(char **av) {
	int shift = (av[1] == NULL ? 1 : a2u(av[1]));
	List *s, *dollarzero;
	if (av[1] != NULL && av[2] != NULL) {
		arg_count("shift");
		return;
	}
	if (shift < 0) {
		badnum(av[1]);
		return;
	}
	s = varlookup("*")->n;
	dollarzero = varlookup("0");
	while (s != NULL && shift != 0) {
		s = s->n;
		--shift;
	}
	if (s == NULL && shift != 0) {
		efprint("cannot shift\n");
		set(false);
	} else {
		varassign("*", append(dollarzero, s), false);
		set(true);
	}
}

/* dud function */

void b_builtin(char **av) {
	USED(av);
}

/* true & false, the closest thing we have to booleans */

static void b_false(char **av) {
	USED(av);
	set(false);
}
static void b_true(char **av) {
	USED(av);
	set(true);
}

/* wait for one or more processes, or all outstanding processes */

static void b_wait(char **av) {
	if (*++av == NULL)
		waitforall();
	else
		waitfor(av);
	sigchk();
}

/*
   whatis without arguments prints all variables and functions. Otherwise, check to see if a name
   is defined as a variable, function or pathname.
*/

#define not(b)	((b)^true)
#define show(b)	(not(eff|vee|pee|bee|ess)|(b))

static bool issig(char *s) {
	int i;
	for (i = 0; i < NUMOFSIGNALS; i++)
		if (streq(s, signals[i].name))
			return true;
	return false;
}

static void b_whatis(char **av) {
	bool ess, eff, vee, pee, bee;
	bool f, found;
	int i, ac, c;
	List *s;
	Node *n;
	char *e;
	for (rc_optind = ac = 0; av[ac] != NULL; ac++)
		; /* count the arguments for getopt */
	ess = eff = vee = pee = bee = false;
	while ((c = rc_getopt(ac, av, "sfvpb")) != -1)
		switch (c) {
		default: set(false); return;
		case 's': ess = true; break;
		case 'f': eff = true; break;
		case 'v': vee = true; break;
		case 'p': pee = true; break;
		case 'b': bee = true; break;
		}
	av += rc_optind;
	if (*av == NULL) {
		if (vee|eff)
			whatare_all_vars(eff, vee);
		if (ess)
			whatare_all_signals();
		if (bee)
			for (i = 0; i < arraysize(builtins); i++)
				fprint(1, "builtin %s\n", builtins[i].name);
		if (pee)
			efprint("whatis -p: must specify argument\n");
		if (show(false)) /* no options? */
			whatare_all_vars(true, true);
		set(true);
		return;
	}
	found = true;
	for (i = 0; av[i] != NULL; i++) {
		f = false;
		errno = ENOENT;
		if (show(vee) && (s = varlookup(av[i])) != NULL) {
			f = true;
			prettyprint_var(1, av[i], s);
		}
		if (((show(ess)&&issig(av[i])) || show(eff)) && (n = fnlookup(av[i])) != NULL) {
			f = true;
			prettyprint_fn(1, av[i], n);
		} else if (show(bee) && isbuiltin(av[i]) != NULL) {
			f = true;
			fprint(1, "builtin %s\n", av[i]);
		} else if (show(pee) && (e = which(av[i], false)) != NULL) {
			f = true;
			fprint(1, "%S\n", e);
		}
		if (!f) {
			found = false;
			if (errno != ENOENT)
				uerror(av[i]);
			else
				efprint("%s not found\n", av[i]);
		}
	}
	set(found);
}

/* push a string to be eval'ed onto the input stack. evaluate it */

static void b_eval(char **av) {
	bool i = interactive;
	if (av[1] == NULL)
		return;
	interactive = false;
	pushstring(av + 1, i); /* don't reset line numbers on noninteractive eval */
	doit(true);
	interactive = i;
}

/*
   push a file to be interpreted onto the input stack. with "-i" treat this as an interactive
   input source.
*/

void b_dot(char **av) {
	int fd;
	bool old_i = interactive, i = false;
	Estack e;
	Edata star;
	av++;
	if (*av == NULL)
		return;
	if (streq(*av, "-i")) {
		av++;
		i = true;
	}
	if (dasheye) { /* rc23 -i file has to do the right thing. reset the dasheye state to false, though. */
		dasheye = false;
		i = true;
	}
	if (*av == NULL)
		return;
	fd = rc_open(*av, rFrom);
	if (fd < 0 && errno == ENOENT && !isabsolute(*av)) {
		/* meh, let's look in $path */
		errno = 0;
		char *f = which(*av, false);
		if (f != NULL)
			fd = rc_open(f, rFrom);
		else
			errno = ENOENT;
	}
	if (fd < 0) {
		delete_cmd(*av);
		uerror(*av);
		set(false);
		return;
	}
	char *old = currfile(*av);
	starassign(*av, av+1, true);
	interactive = i;
	pushfd(fd);
	star.name = "*";
	except(eVarstack, star, &e);
	doit(true);
	varrm("*", true);
	unexcept(eVarstack);
	interactive = old_i;
	currfile(old);
}

/* put rc into a new pgrp. Used on the NeXT where the Terminal program is broken (sigh) */

static void b_newpgrp(char **av) {
	if (av[1] != NULL) {
		arg_count("newpgrp");
		return;
	}
	setpgid(rc_pid, rc_pid); /* XXX check return value */
	tcsetpgrp(2, rc_pid); /* XXX check return value */
}

/* a short but compliant version of plan9port's rfork */
static void b_rfork(char **av) {
	char *flags = av[1] ? av[1] : "ens";

	for (char *f = flags; *f; f++) switch (*f) {
	case 'e':
	case 'E':
	case 'f':
	case 'F':
	case 'm':
	/* 	flag m is mentioned in the rc(1) man page but
		is not handled in the p9p implementation?! */
	case 'n':
	case 'N':
		break;
	case 's':
		if (setpgid(0, getpid())) {
			uerror("rfork: setpgid");
			set(false);
			return;
		}
		break;
	default:
		efprint("rfork: invalid flag '%c'\n", *f);
		set(false);
		return;
	}
}

/* Berkeley limit support was cleaned up by Paul Haahr. */

#if HAVE_SETRLIMIT

#define RLIM_FMT  "%-15s %-15s %s\n"

static void printlimit(Limit *limit, bool hard, bool showdesc) {
	struct rlimit rlim;
	if (getrlimit(limit->flag, &rlim) < 0) {
		uerror("getrlimit");
		return;
	}
	rlim_t lim = hard ? rlim.rlim_max : rlim.rlim_cur;
	char * desc = showdesc ? limit->desc : "";
	if (lim == RLIM_INFINITY) {
		fprint(1, RLIM_FMT, limit->name, "unlimited", desc);
	} else {
		Suffix *suf;
		for (suf = limit->suffix; suf != NULL; suf = suf->next)
			if (lim % suf->amount == 0 && (lim != 0 || suf->amount > 1)) {
				lim /= suf->amount;
				break;
			}
		char *fmt = "%lud%s";
		/* This check must not be done with the cpp since some platforms 
		   define RLIM_IFINITY as (rlim_t)-3, like solaris... */
		if (RLIM_INFINITY > UINT32_MAX)
			fmt = "%qud%s";
		fprint(1, RLIM_FMT, limit->name,
			nprint(fmt, lim, (suf == NULL || lim == 0) ? "" : suf->name),
			desc);
	}
}

static bool parselimit(Limit *resource, rlim_t *limit, char *s) {
	char *t;
	size_t len = strlen(s);
	Suffix *suf = resource->suffix;

	*limit = 1;
	if (len >= 2 && s[0] == 'u' && s[1] == 'n') { /* "unlimited", or "un" for short */
		*limit = RLIM_INFINITY;
		return true;
	}
	if (suf == TIMESUF && (t = strchr(s, ':')) != NULL) {
		int min, sec;
		*t++ = '\0';
		min = a2u(s); sec = a2u(t);
		if (min == -1 || sec == -1) return false;
		*limit = 60 * min + sec;
		return true;
	}
	for (; suf != NULL; suf = suf->next)
		if (streq(suf->name, s + len - strlen(suf->name))) {
			s[len - strlen(suf->name)] = '\0';
			*limit *= suf->amount;
			break;
		}
	int64_t n = a2u64(s);
	if (n < 0) return false;
	*limit *= n;
	return true;
}

static void b_limit(char **av) {
	Limit *lp = limits;
	bool hard = false;
	if (*++av != NULL && streq(*av, "-h")) {
		av++;
		hard = true;
	}
	if (*av == NULL) {
		for (; lp->name != NULL; lp++)
			printlimit(lp, hard, true);
		return;
	}
	for (;; lp++) {
		if (lp->name == NULL) {
			efprint("no such limit\n");
			set(false);
			return;
		}
		if (streq(*av, lp->name))
			break;
	}
	if (*++av == NULL)
		printlimit(lp, hard, false);
	else {
		struct rlimit rlim;
		rlim_t pl;
		if (getrlimit(lp->flag, &rlim) < 0) {
			uerror("getrlimit");
			set(false);
			return;
		}
		if (!parselimit(lp, &pl, *av)) {
			efprint("bad limit\n");
			set(false);
			return;
		}
		if (hard)
			rlim.rlim_max = pl;
		else
			rlim.rlim_cur = pl;
		if (setrlimit(lp->flag, &rlim) < 0) {
			uerror("setrlimit");
			set(false);
		} else
			set(true);
	}
}
#endif

char *compl_builtin(const char *text, int state) {
	return compl_name(text, state, &builtins[0].name, arraysize(builtins), &builtins[1].name - &builtins[0].name);
}
