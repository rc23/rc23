#include "rc.h"

void dump(Node *, int);

/* dump a Node * as a tree */

static void dump_1(char *p, Node *n, int indent) {
	fprint(2, "%s:\n", p);
	dump(n->u[0].p, indent + 1);
}

static void dump_2(char *p, Node *n, int indent) {
	fprint(2, "%s:\n", p);
	dump(n->u[0].p, indent + 1);
	if (n->u[1].p != NULL)
		dump(n->u[1].p, indent + 1);
}

static void dump_3(char *p, Node *n, int indent) {
	fprint(2, "%s:\n", p);
	dump(n->u[0].p, indent + 1);
	if (n->u[1].p != NULL)
		dump(n->u[1].p, indent + 1);
	if (n->u[2].p != NULL)
		dump(n->u[2].p, indent + 1);
}

static void dump_s(char *p, Node *n, int indent) {
	USED(indent);
	fprint(2, "%s: %S\n", p, n->u[0].s);
}

static void dump_pipe(char *p, Node *n, int indent) {
	int ifd = n->u[0].i, ofd = n->u[1].i;
	fprint(2, "%s %d %d:\n", p, ifd, ofd);
	dump(n->u[2].p, indent + 1);
	dump(n->u[3].p, indent + 1);
}

static void dump_fnargs(char *p, Node *n, int indent) {
	int i = -1;
	fprint(2, "%s:\n", p);
	for (; i < indent; ++i)
		fprint(2, "    ");
	fprint(2, "%d\n", n->u[0].i);
	dump(n->u[1].p, indent + 1);
}

static char *redir(int op) {
	switch (op) {
	case rCreate: return ">";
	case rAppend: return ">>";
	case rFrom: return "<";
	case rHeredoc: return "<<";
	case rHerestring: return "<<<";
	default: return "?";
	}
}
static void dump_dup(char *p, Node *n, int indent) {
	USED(indent);
	fprint(2, "%s %s %d %d:\n", p, redir(n->u[0].i), n->u[1].i, n->u[2].i);
}

static void dump_redir(char *p, Node *n, int indent) {
	fprint(2, "%s %s %d:\n", p, redir(n->u[0].i), n->u[1].i);
	dump(n->u[2].p, indent + 1);
}

void dump(Node *n, int indent) {
	int i;

	if (n == NULL)
		return;

	for (i = 0; i < indent; ++i)
		fprint(2, "    ");

	switch (n->type) {
	case nAndalso:
		dump_2("nAndalso", n, indent);
		break;
	case nArgs:
		dump_2("nArgs", n, indent);
		break;
	case nAssign:
		dump_2("nAssign", n, indent);
		break;
	case nBackq:
		dump_1("nBackq", n, indent);
		break;
	case nBang:
		dump_1("nBang", n, indent);
		break;
	case nBody:
		dump_2("nBody", n, indent);
		break;
	case nBrace:
		dump_1("nBrace", n, indent);
		break;
	case nCase:
		dump_1("nCase", n, indent);
		break;
	case nCbody:
		dump_2("nCbody", n, indent);
		break;
	case nConcat:
		dump_2("nConcat", n, indent);
		break;
	case nCount:
		dump_1("nCount", n, indent);
		break;
	case nDup:
		dump_dup("nDup", n, indent);
		break;
	case nElse:
		dump_2("nElse", n, indent);
		break;
	case nEpilog:
		dump_2("nEpilog", n, indent);
		break;
	case nFlat:
		dump_1("nFlat", n, indent);
		break;
	case nFnargs:
		dump_fnargs("nFnargs", n, indent);
		break;
	case nForin:
		dump_3("nForin", n, indent);
		break;
	case nIf:
		dump_2("nIf", n, indent);
		break;
	case nIfnot:
		dump_1("nIfnot", n, indent);
		break;
	case nLappend:
		dump_2("nLappend", n, indent);
		break;
	case nMatch:
		dump_2("nMatch", n, indent);
		break;
	case nNewfn:
		dump_2("nNewfn", n, indent);
		break;
	case nNmpipe:
		dump_redir("nNowait", n, indent);
		break;
	case nNowait:
		dump_1("nNowait", n, indent);
		break;
	case nOrelse:
		dump_2("nNowait", n, indent);
		break;
	case nPipe:
		dump_pipe("nPipe", n, indent);
		break;
	case nPre:
		dump_2("nPre", n, indent);
		break;
	case nRedir:
		dump_redir("nRedir", n, indent);
		break;
	case nRmfn:
		dump_1("nRmfn", n, indent);
		break;
	case nSubshell:
		dump_1("nSubshell", n, indent);
		break;
	case nSwitch:
		dump_2("nSwitch", n, indent);
		break;
	case nVar:
		dump_1("nVar", n, indent);
		break;
	case nVarsub:
		dump_2("nVarsub", n, indent);
		break;
	case nWhile:
		dump_2("nWhile", n, indent);
		break;
	case nWord:
		dump_s("nWord", n, indent);
		break;
	default:
		fprint(2, "unknown\n");
		break;
	}
}

void tree_dump(Node *f) {
	dump(f, 0);
}
