/* utils.c: functions of general utility */

#include "rc.h"

#include <errno.h>
#include <limits.h>

/* our perror */

void uerror(char *s) {
	char *err;

	err = strerror(errno);
	if (!err) err = "unknown error";

	if (s)
		efprint("%s: %s\n", s, err);
	else
		efprint("%s\n", err);
}

/* Die horribly. This should never get called. Please let me know if it does. */

#define PANICMSG "rc23 panic: "

void panic(char *s) {
	(void)! write(2, PANICMSG, conststrlen(PANICMSG));
	(void)! write(2, s, strlen(s));
	(void)! write(2, "!\n", 2);
	exit(1);
}

/* ascii -> unsigned conversion routines. -1 indicates conversion error. */

int n2u(char *s, unsigned int base) {
	unsigned int i, j;
	for (i = 0; *s != '\0'; s++) {
		j = (unsigned int) *s - '0';
		if (j >= base) /* small hack with unsigned ints -- one compare for range test */
			return -1;
		j = i * base + j;
		if (j < i || j > INT_MAX) /* overflow */
			return -1;
		i = j;
	}
	return (int) i;
}

int64_t n2u64(char *s, unsigned int base) {
	uint64_t i, j;
	for (i = 0; *s != '\0'; s++) {
		j = (unsigned int) *s - '0';
		if (j >= base)
			return -1;
		j = i * base + j;
		if (j < i || j > INT64_MAX)
			return -1;
		i = j;
	}
	return (int64_t) i;
}

/* The last word in portable ANSI: a strcmp wrapper for qsort */

int starstrcmp(const void *s1, const void *s2) {
	return strcmp(*(char * const *)s1, *(char * const *)s2);
}

/* tests to see if pathname begins with "/", "./", or "../" */

bool isabsolute(char *path) {
	return path[0] == '/' || (path[0] == '.' && (path[1] == '/' || (path[1] == '.' && path[2] == '/')));
}


/* duplicate a fd and close the old one only if necessary */

int mvfd(int i, int j) {
	if (i != j) {
		int s = dup2(i, j);
		close(i);
		return s;
	}
	return 0;
}
