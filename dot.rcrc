# Example configuration for rc23. Copy this file into $home/.rcrc
# Intensionally left compatible with byron's rc.

echo $0 $version(1) !$pid

switch ($0) {
  case ?rc23; SHELL = `{whatis -p rc23}
  case ?rc; SHELL = `{whatis -p rc}
}


if (!~ $PWD ()) {
  cwd = $PWD
  PWD = ()
} else cwd = `` $nl {builtin pwd}

# ANSI Codes
ansi_esc = `{printf '\033'}
ansi_np  = `{printf '\\'}

# Pager
PAGER = more
if (whatis -p less >/dev/null >[2=1]) {
  PAGER = less
  LESSHISTFILE = /dev/null
}

# Editor
EDITOR = vi
if (whatis -p vim >/dev/null >[2=1]) {
  EDITOR = vim
}
VISUAL = $EDITOR

# Functions

fn .. { cd .. }
fn apply { f=$1 i=() { shift; for (i) $f $i }}
fn bc { builtin bc -lq $* }
fn bye { echo Goodbye!; exit 0 }
fn cd {
  switch ($#*) {
  case 0
    if (!~ $cwd $home)
      builtin cd && { oldcwd = $cwd; cwd = $home }
  case 1
    if (~ $1 -) {
      _t = $cwd
      builtin cd $oldcwd && {
        cwd = $oldcwd
        oldcwd = $_t; _t=()
        echo -- $cwd
      }
    } else {
      builtin cd $1 && {
        oldcwd = $cwd
        if (~ $1 /*) {
          cwd = $1
        } else cwd = `` $nl {builtin pwd}
      }
    }
  case *
    echo $0: too many arguments >[1=2]
    return 2
  }
}
fn chx { chmod +x $* }
fn disapproval { echo ಠ_ಠ }
# avoid printing newlines to not break sort(1)
fn envv { nl=\n ifs=... whatis -v | sort -t = -k 1 }
fn mkcd { mkdir -p $1 && cd $1 }
fn or { echo -- $1 }
fn pwd {
  if (!~ $#* 0) { builtin pwd $*; return }
  if (~ $cwd ()) cwd = `` $nl {builtin pwd}
  echo -- $cwd
}
fn winch { * = `resize; eval $1 $2; dowinch=() }
# Like the x operator in Perl (repeat a character n times)
fn x { c=$1 n=$2 {
  ~ $c && return 1
  printf $c^'%.0s' `{seq `{or $n $COLUMNS 80}}
  echo
}}


if (builtin ls --version >/dev/null >[2=1]) {
  # GNU ls(1)
  fn ls {
    flag i && * = (-N --group-directories-first '--color=auto' $*)
    builtin ls $*
  }
  fn ll { ls -lahF '--time-style=long-iso' $* }
  fn la { ls -aF $* }
  fn l  { ls -CF $* }
  fn grep { builtin grep '--color=auto' $* }
} else {
  # other ls(1)
  fn ll { ls -lahF $* }
  fn la { ls -aF $* }
  fn l  { ls -CF $* }
}

# Disable stupid ctrl+s
if (test -t 1) stty -ixon

# Handle window size changes
fn sigwinch { dowinch=1 }
winch

if (!~ $SSH_CONNECTION ()) {
  HOST = `{hostname -s}
  prompt = ($HOST' ; ' '')
}

fn prompt { s = $status {
  # Modify the terminal's window title.
  # Work extra hard to avoid forking here.
  echo -n $ansi_esc']2;' rc : $HOST $cwd $ansi_esc$ansi_np
  # Show $status in case of error.
  ~ $s [~0]* && echo '(status '$"s')'
  # Make it clear we are root.
  ~ $USER root && prompt = ('root! ' '')
  # Update $COLUMNS and $LINES if needed.
  ~ $dowinch 1 && winch
}}

echo
