/*
  The idea of this file is to include prototypes for all external
  functions that rc uses, either by including the appropriate header
  file, or---for older systems---declaring the functions directly.
*/

/* rc23 requires C99 and a POSIX.1-2001 compliant system. */
#define _XOPEN_SOURCE 600

#ifndef _FILE_OFFSET_BITS
# define _FILE_OFFSET_BITS 64
#endif

#if defined(__APPLE__) && !defined(_DARWIN_USE_64_BIT_INODE)
# define _DARWIN_USE_64_BIT_INODE 1
#endif

#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if defined(_POSIX_VERSION) && _POSIX_VERSION >= 199506L
/* Only support POSIX setrlimit */
#define HAVE_SETRLIMIT 1
#endif

#ifndef __dead
#if __GNUC__ || __clang__ || __TINYC__ >= 927 || __SUNPRO_C
#define __dead __attribute__((__noreturn__))
#else
#define __dead
#endif
#endif /* __dead */

#define Q(...) #__VA_ARGS__

#if __GNUC__
#define CC_DIAG(...) _Pragma(Q(GCC diagnostic __VA_ARGS__))
#elif __clang__
#define CC_DIAG(...) _Pragma(Q(clang diagnostic __VA_ARGS__))
#else
#define CC_DIAG(...)
#endif

#if __GNUC__ >= 13
 #define NO_WARN(x)   \
   CC_DIAG(push)      \
   CC_DIAG(ignored x)
 #define NO_WARN_END CC_DIAG(pop)
#else
 #define NO_WARN(x)
 #define NO_WARN_END
#endif
