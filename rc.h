#undef NDEBUG
#include "config.h"
#include "proto.h"
#include "version.h"

#include <assert.h>
#include <setjmp.h>

/* for struct stat */
#include <sys/stat.h>

#define RC "rc23: "

/* datatypes */

#define ENV_SEP '\001'
#define ENV_ESC '\002'

typedef void builtin_t(char **);
typedef struct Block Block;
typedef struct Dup Dup;
typedef struct Estack Estack;
typedef struct Func Func;
typedef struct Hq Hq;
typedef struct Htab Htab;
typedef struct Jbwrap Jbwrap;
typedef struct List List;
typedef struct Node Node;
typedef struct Pipe Pipe;
typedef struct Redir Redir;
typedef struct Rq Rq;
typedef struct Variable Variable;
typedef struct Word Word;
typedef struct Format Format;
typedef union Edata Edata;

typedef enum nodetype {
	nAndalso, nAssign, nBackq, nBang, nBody, nCbody, nNowait, nBrace,
	nConcat, nCount, nElse, nFlat, nDup, nEpilog, nNewfn, nForin, nIf,
	nIfnot, nOrelse, nPipe, nPre, nRedir, nRmfn, nArgs, nSubshell, nCase,
	nSwitch, nMatch, nVar, nVarsub, nWhile, nWord, nLappend, nNmpipe, nFnargs
} nodetype;

typedef enum ecodes {
	eError, eBreak, eReturn, eVarstack, eArena, eFifo, eFd, eContinue
} ecodes;

typedef enum redirtype {
	rFrom, rCreate, rAppend, rHeredoc, rHerestring
} redirtype;

typedef bool (*Conv)(Format *, int);

union Edata {
	Jbwrap *jb;
	Block *b;
	char *name;
	int fd;
};

struct Estack {
	Edata data;
	Estack *prev;
	ecodes e;
	bool interactive;
};

/* Certain braindamaged environments don't define jmp_buf
   as an array, so wrap it in a structure. */
struct Jbwrap {
	sigjmp_buf j;
};

struct List {
	char *w, *m;
	List *n;
};

struct Node {
	nodetype type;
	union {
		char *s;
		int i;
		Node *p;
	} u[4];
};

struct Pipe {
	int left, right;
};

struct Dup {
	redirtype type;
	int left, right;
};

struct Redir {
	redirtype type;
	int fd;
};

struct Word {
	char *w, *m;
	bool q;
};

struct Rq {
	Node *r;
	struct Rq *n;
};

struct Func {
	Node *def;
	char *extdef;
};

struct Variable {
	List *def;
	char *extdef;
	Variable *n;
};

struct Htab {
	char *name;
	void *p;
};

struct Format {
	/* for the formatting routines */
	va_list args;
	long flags, f1, f2;
	/* for the buffer maintenance routines */
	char *buf, *bufbegin, *bufend;
	void (*grow)(Format *, size_t);
	union { int n; void *p; } u;
	ssize_t flushed;
};

/* Format->flags values */
enum {
	FMT_quad	= 1,		/* %q */
	FMT_long	= 2,		/* %l */
	FMT_unsigned	= 8,		/* %u */
	FMT_zeropad	= 16,		/* %0 */
	FMT_leftside	= 32,		/* %- */
	FMT_altform	= 64,		/* %# */
	FMT_f1set	= 128,		/* %<n> */
	FMT_f2set	= 256		/* %.<n> */
};

/* macros */
#define EOF (-1)
#define MAX_PIPELINE 512
#ifndef NULL
#define NULL 0
#endif
#define a2u(x) n2u(x, 10)
#define o2u(x) n2u(x, 8)
#define a2u64(x) n2u64(x, 10)
#define arraysize(a) ((int)(sizeof(a)/sizeof(*a)))
#define memzero(s, n) memset(s, 0, n)
#define enew(x) ((x *) ealloc(sizeof(x)))
#define nnew(x) ((x *) nalloc(sizeof(x)))
#define lookup_fn(s) ((Func *) lookup(s, fp))
#define lookup_var(s) ((Variable *) lookup(s, vp))
#define lookup_cmd(s) ((char *) lookup(s, cp))
#ifndef offsetof
#define offsetof(t, m) ((size_t) (((char *) &((t *) 0)->m) - (char *)0))
#endif
#define streq(x, y) (*(x) == *(y) && strcmp(x, y) == 0)
#define conststrlen(x) (sizeof (x) - 1)
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define USED(x) ((void)(x))

/* rc prototypes */

/* main.c */
extern Rq *redirq;
extern bool dashdee, dashee, dasheye, dashell, dashen, dashoh;
extern bool dashpee, dashess, dashTEE, dashyew, dashvee, dashex;
extern bool interactive;
extern char *dashsee[];
extern pid_t rc_pid;

/* builtins.c */
builtin_t *isbuiltin(char *);
void b_exec(char **), funcall(char **), b_dot(char **), b_builtin(char **);
char *compl_builtin(const char *, int);

/* develop.c */
void tree_dump(Node *);

/* except.c */
extern bool nl_on_intr;
bool outstanding_cmdarg(void);
void pop_cmdarg(bool);
void rc_raise(ecodes);
void except(ecodes, Edata, Estack *);
void unexcept(ecodes);
void clearflow(void);
void rc_error(char *) __dead;
void sigint(int);

/* exec.c */
void exec(List *, bool);
#if HASH_BANG
#define rc_execve execve
#else
int rc_execve(char *, char **, char **);
#endif

/* footobar.c */
char **list2array(List *, bool);
char *get_name(char *);
List *parse_var(char *);
Node *parse_fn(char *);
void initprint(void);

/* fn.c */
void inithandler(void);
void setsigdefaults(bool);
void rc_exit(int) __dead;
void fnassign(char *, Node *);
void fnassign_string(char *);
Node *fnlookup(char *);
char *fnlookup_string(char *);
void fnrm(char *);
void whatare_all_signals(void);
void prettyprint_fn(int, char *, Node *);

/* getopt.c */
int rc_getopt(int, char **, char *);

extern int rc_optind, rc_opterr, rc_optopt;
extern char *rc_optarg;

/* glob.c */
bool lmatch(List *, List *);
List *glob(List *);

/* glom.c */
void assign(List *, List *, bool);
void qredir(Node *);
List *append(List *, List*);
List *append_noalloc(List *, List*);
bool tryappend(List *, Node *);
List *flatten(List *);
List *glom(Node *);
List *glom_safe(Node *);
List *concat(List *, List *);
List *varsub(List *, List *);
List *word(char *, char *);

/* hash.c */
extern Htab *fp, *vp, *cp;
void *lookup(char *, Htab *);
Func *get_fn_place(char *);
Variable *get_var_place(char *, bool);
void set_cmd_path(char *, char *);
char **makeenv(void);
void delete_fn(char *);
void delete_var(char *, bool);
void delete_cmd(char *);
void reset_cmdtab(void);
void initenv(char **);
void inithash(void);
void set_exportable(char *, bool);
void whatare_all_vars(bool, bool);
char *compl_name(const char *, int, char **, size_t, ssize_t);
char *compl_fn(const char *, int);
char *compl_var(const char *, int);

/* heredoc.c */
int heredoc(int);
int qdoc(Node *, Node *);
extern Hq *hq;

/* lex.c */
bool quotep(char *, bool);
int yylex(void);
void inityy(void);
void yyerror(const char *);
void scanerror(char *);
extern int lineno;
extern const char nw[], dnw[];

/* list.c */
void listfree(List *);
List *listcpy(List *, void *(*)(size_t));
size_t listlen(List *);
int listnel(List *);

/* match.c */
bool match(char *, char *, char *);

/* alloc.c */
void *ealloc(size_t);
void *ecalloc(size_t, size_t);
void *erealloc(void *, size_t);
void efree(void *);
Block *newblock(void);
void *nalloc(size_t);
void nfree(void);
void restoreblock(Block *);

/* open.c */
int rc_open(const char *, redirtype);
int rc_open_perm(const char *, redirtype, mode_t);
bool makeblocking(int);
bool makesamepgrp(int);

/* print.c */
Conv fmtinstall(int, Conv);
ssize_t printfmt(Format *, const char *);
ssize_t fmtprint(Format *, const char *, ...);
void fmtappend(Format *, const char *, size_t);
void fmtcat(Format *, const char *);
void fmtputc(Format *, int);
ssize_t fprint(int fd, const char *fmt, ...);
void efprint(const char *, ...);
char *currfile(char *);
char *mprint(const char *fmt, ...);
char *nprint(const char *fmt, ...);

/* parse.c (parse.y) */
extern Node *parsetree, *empty;
int yyparse(void);
void initparse(void);

/* readline */
extern volatile sig_atomic_t rl_active;
extern struct Jbwrap rl_buf;

/* redir.c */
void doredirs(void);


/* signal.c */
void initsignal(void);
void catcher(int);
void sigchk(void);
void (*rc_signal(int, void (*)(int)))(int);
void (*sys_signal(int, void (*)(int)))(int);
extern void (*sighandlers[])(int);


/* status.c */
int istrue(void);
int getstatus(void);
void set(bool);
void setstatus(pid_t, int);
void setpipestatuslength(int);
void setpipestatus(int, pid_t, int);
List *sgetstatus(void);
void ssetstatus(char **);
char *strstatus(int s);


/* system.c or system-bsd.c */
void writeall(int, char *, size_t);

#define rc_read read
#define rc_wait wait


/* tree.c */
Node *mk(enum nodetype, ...);
Node *treecpy(Node *, void *(*)(size_t));
void treefree(Node *);
Node *link_fn_args(Node *, Node *);
Node *mkwordi(int);

/* utils.c */
bool isabsolute(char *);
int n2u(char *, unsigned int);
int64_t n2u64(char *, unsigned int);
int mvfd(int, int);
int starstrcmp(const void *, const void *);
void panic(char *) __dead;
void uerror(char *);

/* var.c */
void varassign(char *, List *, bool);
bool varappend(char *, List *);
bool varassign_string(char *);
List *varlookup(char *);
List *varlookup_safe(char *);
char *varlookup_string(char *);
void varrm(char *, bool);
void starassign(char *, char **, bool);
void alias(char *, List *, bool);
void prettyprint_var(int, char *, List *);

/* wait.c */
pid_t rc_fork(void);
pid_t rc_wait4(pid_t, int *, bool);
List *sgetapids(void);
void waitforall(void);
void waitfor(char **);
extern bool forked;

/* walk.c */
bool walk(Node *, bool);
extern bool cond;

/* which.c */
bool rc_access(char *, bool, struct stat *);
char *which(char *, bool);
void verify_cmd(char *);

/* inline functions */

/* strdup using ealloc */
static inline char *ecpy(char *src) {
	size_t sz = strlen(src) + 1;
	return memcpy(ealloc(sz), src, sz);
}

/* strdup using nalloc */
static inline char *ncpy(char *src) {
	size_t sz = strlen(src) + 1;
	return memcpy(nalloc(sz), src, sz);
}
