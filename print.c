/* print.c -- formatted printing routines (Paul Haahr, 12/91) */

#include "rc.h"

#define	PRINT_ALLOCSIZE	((size_t)64)
#define	SPRINT_BUFSIZ	((size_t)1024)

#define	MAXCONV 256

/* current file being evaluated, used in error messages */
static char *filename;

/*
 * conversion functions
 *	true return -> flag changes only, not a conversion
 */

#define Flag(name, flag) \
static bool name(Format *format, int c) { \
	USED(c); \
	format->flags |= flag; \
	return true; \
}

Flag(uconv,	FMT_unsigned)
Flag(rc_lconv,	FMT_long)
Flag(qconv,	FMT_quad)
Flag(altconv,	FMT_altform)
Flag(leftconv,	FMT_leftside)
Flag(dotconv,	FMT_f2set)

static bool digitconv(Format *format, int c) {
	if (format->flags & FMT_f2set)
		format->f2 = 10 * format->f2 + c - '0';
	else {
		format->flags |= FMT_f1set;
		format->f1 = 10 * format->f1 + c - '0';
	}
	return true;
}

static bool zeroconv(Format *format, int c) {
	USED(c);
	if (format->flags & (FMT_f1set | FMT_f2set))
		return digitconv(format, '0');
	format->flags |= FMT_zeropad;
	return true;
}

static void pad(Format *format, size_t len, int c) {
	while (len-- != 0)
		fmtputc(format, c);
}

static bool sconv(Format *format, int c) {
	USED(c);
	char *s = va_arg(format->args, char *);
	if ((format->flags & FMT_f1set) == 0)
		fmtcat(format, s);
	else {
		size_t len = strlen(s), width = format->f1 - len;
		if (format->flags & FMT_leftside) {
			fmtappend(format, s, len);
			pad(format, width, ' ');
		} else {
			pad(format, width, ' ');
			fmtappend(format, s, len);
		}
	}
	return false;
}

static char *rc_utoa(uint64_t u, char *t, unsigned int radix, const char *digit) {
	if (u >= radix) {
		t = rc_utoa(u / radix, t, radix, digit);
		u %= radix;
	}
	*t++ = digit[u];
	return t;
}

static void intconv(Format *format, unsigned int radix, int upper, const char *altform) {
	static const char * const table[] = {
		"0123456789abcdefghijklmnopqrstuvwxyz",
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",
	};
	char padchar;
	size_t len, pre, zeroes, padding, width;
	int64_t n;
	uint64_t u;
	char number[64], prefix[20];

	if (radix > 36)
		return;

	long flags = format->flags;

	if (flags & FMT_quad)
		n = va_arg(format->args, int64_t);
	else if (flags & FMT_long)
		n = va_arg(format->args, long);
	else
		n = va_arg(format->args, int);

	pre = 0;
	if ((flags & FMT_unsigned) || n >= 0)
		u = n;
	else {
		prefix[pre++] = '-';
		u = -n;
	}

	if (flags & FMT_altform)
		while (*altform != '\0')
			prefix[pre++] = *altform++;

	len = rc_utoa(u, number, radix, table[upper]) - number;
	if ((flags & FMT_f2set) && (size_t) format->f2 > len)
		zeroes = format->f2 - len;
	else
		zeroes = 0;

	width = pre + zeroes + len;
	if ((flags & FMT_f1set) && (size_t) format->f1 > width) {
		padding = format->f1 - width;
	} else
		padding = 0;

	padchar = ' ';
	if (padding > 0 && flags & FMT_zeropad) {
		padchar = '0';
		if ((flags & FMT_leftside) == 0) {
			zeroes += padding;
			padding = 0;
		}
	}


	if ((flags & FMT_leftside) == 0)
		pad(format, padding, padchar);
	fmtappend(format, prefix, pre);
	pad(format, zeroes, '0');
	fmtappend(format, number, len);
	if (flags & FMT_leftside)
		pad(format, padding, padchar);
}

static bool cconv(Format *format, int c) {
	USED(c);
	fmtputc(format, va_arg(format->args, int));
	return false;
}

static bool dconv(Format *format, int c) {
	USED(c);
	intconv(format, 10, 0, "");
	return false;
}

static bool oconv(Format *format, int c) {
	USED(c);
	intconv(format, 8, 0, "0");
	return false;
}

static bool xconv(Format *format, int c) {
	USED(c);
	intconv(format, 16, 0, "0x");
	return false;
}

static bool pctconv(Format *format, int c) {
	USED(c);
	fmtputc(format, '%');
	return false;
}

static bool badconv(Format *ignore, int ign0re) {
	USED(ignore); USED(ign0re);
	panic("bad conversion character in printfmt");
	/* NOTREACHED */
}


/*
 * conversion table management
 */

static Conv fmttab[MAXCONV];

static void inittab(void) {
	int i;
	for (i = 0; i < MAXCONV; i++)
		fmttab[i] = badconv;

	fmttab['s'] = sconv;
	fmttab['c'] = cconv;
	fmttab['d'] = dconv;
	fmttab['o'] = oconv;
	fmttab['x'] = xconv;
	fmttab['%'] = pctconv;
	fmttab['q'] = qconv;
	fmttab['u'] = uconv;
	fmttab['l'] = rc_lconv;
	fmttab['#'] = altconv;
	fmttab['-'] = leftconv;
	fmttab['.'] = dotconv;

	fmttab['0'] = zeroconv;
	for (i = '1'; i <= '9'; i++)
		fmttab[i] = digitconv;
}

bool (*fmtinstall(int c, bool (*f)(Format *, int)))(Format *, int) {
/*Conv fmtinstall(int c, Conv f) {*/
	Conv oldf;
	if (fmttab[0] == NULL)
		inittab();
	c &= MAXCONV - 1;
	oldf = fmttab[c];
	if (f != NULL)
		fmttab[c] = f;
	return oldf;
}


/*
 * functions for inserting strings in the format buffer
 */

void fmtappend(Format *format, const char *s, size_t len) {
	size_t free;

	while ((free = format->bufend - format->buf) < len) {
		memcpy(format->buf, s, free);
		format->buf += free;
		s += free;
		len -= free;
		(*format->grow)(format, len);
	}
	memcpy(format->buf, s, len);
	format->buf += len;
}

void fmtcat(Format *format, const char *s) {
	fmtappend(format, s, strlen(s));
}

void fmtputc(Format *format, int c) {
	if (format->buf >= format->bufend)
		(*format->grow)(format, (size_t)1);
	*(format->buf++) = c;
}

/*
 * printfmt -- the driver routine
 */

ssize_t printfmt(Format *format, const char *fmt) {
	unsigned const char *s = (unsigned const char *) fmt;

	if (fmttab[0] == NULL)
		inittab();

	for (;;) {
		int c = *s++;
		switch (c) {
		case '%':
			format->flags = format->f1 = format->f2 = 0;
			do
				c = *s++;
			while ((*fmttab[c])(format, c));
			break;
		case '\0':
			return format->buf - format->bufbegin + format->flushed;
		default:
			fmtputc(format, c);
			break;
		}
	}
}


/*
 * the public entry points
 */

NO_WARN("-Wanalyzer-va-list-leak")

ssize_t fmtprint(Format *format, const char *fmt, ...) {
	ptrdiff_t n = -format->flushed;
	va_list saveargs;

	va_copy(saveargs, format->args);
	va_start(format->args, fmt);
	n += printfmt(format, fmt);
	va_end(format->args);
	va_copy(format->args, saveargs); /* -fanalyzer sees a leak */
	va_end(saveargs);

	return n + format->flushed;
}

NO_WARN_END

static void fprint_flush(Format *format, size_t ignore) {
	char *beg = format->bufbegin;
	ptrdiff_t n = format->buf - beg;
	USED(ignore);

	format->flushed += n;
	format->buf = beg;
	writeall(format->u.n, beg, (size_t)n);
}

ssize_t fprint(int fd, const char *fmt, ...) {
	char buf[1024];
	Format format;

	format.buf	= buf;
	format.bufbegin	= buf;
	format.bufend	= buf + sizeof buf;
	format.grow	= fprint_flush;
	format.flushed	= 0;
	format.u.n	= fd;

	va_start(format.args, fmt);
	printfmt(&format, fmt);
	va_end(format.args);

	fprint_flush(&format, 0);
	return format.flushed;
}

/* print error with line number on noninteractive shells (i.e., scripts) */
void efprint(const char *fmt, ...) {
	char buf[1024];
	Format format;

	memzero(&format, sizeof(Format));
	format.buf	= buf;
	format.bufbegin	= buf;
	format.bufend	= buf + sizeof buf;
	format.grow	= fprint_flush;
	format.u.n	= 2;

	if (interactive)
		fmtcat(&format, RC);
	else
		fmtprint(&format, RC "%s:%d: ", filename, lineno - 1);

	va_start(format.args, fmt);
	printfmt(&format, fmt);
	va_end(format.args);

	fprint_flush(&format, 0);
}

/* Set the current file being evaluated and return the previous one
   if name is not null, else return the current file.  */
char *currfile(char *name) {
	char *old = filename;
	if (name != NULL)
		filename = name;
	return old;
}

static void memprint_grow(Format *format, size_t more) {
	char *buf;
	ptrdiff_t _len = format->bufend - format->bufbegin + 1;
	ptrdiff_t used = format->buf - format->bufbegin;

	assert(_len >= 0);
	size_t len = (size_t)_len;

	len = (len >= more)
		? len * 2
		: ((len + more) + PRINT_ALLOCSIZE) &~ (PRINT_ALLOCSIZE - 1);
	if (format->u.n)
		buf = erealloc(format->bufbegin, len);
	else {
		buf = nalloc(len);
		memcpy(buf, format->bufbegin, used);
	}
	format->buf = buf + used;
	format->bufbegin = buf;
	format->bufend = buf + len - 1;
}

static char *memprint(Format *format, const char *fmt, char *buf, size_t len) {
	format->buf	 = buf;
	format->bufbegin = buf;
	format->bufend	 = buf + len - 1;
	format->grow	 = memprint_grow;
	format->flushed	 = 0;
	printfmt(format, fmt);
	*format->buf = '\0';
	return format->bufbegin;
}

char *mprint(const char *fmt,...) {
	Format format;
	char *result;
	va_list ap;

	format.u.n = 1;
	va_start(ap, fmt);
	va_copy(format.args, ap);
	result = memprint(&format, fmt, ealloc(PRINT_ALLOCSIZE), PRINT_ALLOCSIZE);
	va_end(format.args);
	return result;
}

char *nprint(const char *fmt,...) {
	Format format;
	char *result;
	va_list ap;

	format.u.n = 0;
	va_start(ap, fmt);
	va_copy(format.args, ap);
	result = memprint(&format, fmt, nalloc(PRINT_ALLOCSIZE), PRINT_ALLOCSIZE);
	va_end(format.args);
	return result;
}
