#include "rc.h"

#include <errno.h>

#include "edit.h"

bool editing = 0;

void *edit_begin(int fd) {
	USED(fd);
	assert(0); /* should never be called */
	return NULL;
}

char *edit_alloc(void *cookie, size_t *count) {
	USED(cookie); USED(count);
	assert(0); /* should never be called */
	return NULL;
}

void edit_prompt(void *cookie, char *prompt) {
	USED(cookie); USED(prompt);
	assert(0); /* should never be called */
}

void edit_free(void *buffer) {
	USED(buffer);
	assert(0); /* should never be called */
}

void edit_end(void *cookie) {
	USED(cookie);
	assert(0); /* should never be called */
}

void edit_reset(void *cookie) {
	USED(cookie);
	assert(0); /* should never be called */
}
