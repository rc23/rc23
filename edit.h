extern bool editing;

void *edit_begin(int fd);

char *edit_alloc(void *, size_t *);
void edit_free(void *);

void edit_prompt(void *, char *);

void edit_end(void *);

void edit_reset(void *);

/* Check if a line should be added to the history. */
static inline bool hist_allowed(char s[static 1]) {
  return s[0] != ' ';
}
